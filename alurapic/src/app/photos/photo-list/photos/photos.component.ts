import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { Photo } from '../../photo/photo';

@Component({
  selector: 'ap-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnChanges { // 

  @Input() photos1: Photo[] = []; // @Input esse decorator diz que photos é inbound property, ou seja, o template aceita receber os valores
  rows: any[] = [];
  
  constructor() { }
  
  ngOnChanges(changes: SimpleChanges): void { // Recebo como parâmetro todas as possíveis mudanças das inbound property do componente
    if(changes.photos1) { // "changes você tem a propriedade photos?" (Um objeto do tipo SimpleChanges possui uma propriedade de mesmo nome da inbound property que sofreu mudança)
      this.rows = this.groupColumns(this.photos1); // Se tem eu sei que ela mudou, então executa o this.groupColumns
    }
  }

  groupColumns(photos: Photo[]) {
    const newRows = [];

    for(let index = 0; index <= photos.length; index+=3) {
      newRows.push(photos.slice(index, index + 3)); 
    }               // Exemplo:   0        3 . O slice vai pegar uma fatia do array, então 0, 1, 2. Na próxima vez será
                    //            3        6 e os itens serão 3, 4, 5.

    return newRows;
  }

}
