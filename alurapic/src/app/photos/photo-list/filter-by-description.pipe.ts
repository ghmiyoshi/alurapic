import { Pipe, PipeTransform } from "@angular/core";

import { Photo } from "../photo/photo";

@Pipe({ name: "filterByDescription" }) // Nome do pipe que vou utilizar no html
export class FilterByDescription implements PipeTransform { // Implemento PipeTransform para garantir a implementação do método transform()

    transform(photos: Photo[], descriptionQuery: string) { // Transformo uma lista de fotos e o que recebo no input é uma string
        descriptionQuery = descriptionQuery.trim().toLowerCase() // trim para garantir tirar todos os espaços e lowerCase para considerar tudo em minusculo 

        if (descriptionQuery) {
            return photos.filter(photos => // Pego a lista de fotos, para cada foto pego a descrição, transformo em minusculo e verifico se faz parte dessa string o que digitei. Se faz, entra na lista de filter, se não, retorno a lista photos
                photos.description.toLowerCase().includes(descriptionQuery)
            )
        } else {
            return photos;
        }
    }

}