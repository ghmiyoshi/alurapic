import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Photo } from '../photo/photo';
import { PhotoService } from '../photo/photo.service';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})
export class PhotoListComponent implements OnInit { // Implemento OnInit e OnDestroy para implementar seus métodos
  
  photos2: Photo[] = []; // Como o arrow function photos é do tipo Object, preciso declarar o tipo da variável como array de Object 
  filter: string = '';
  hasMore: boolean = true;
  currentPage: number = 1;
  userName: string = '';
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private photoService: PhotoService
  ) {} // constructor apenas para injeção de dependências
  
  
  ngOnInit(): void { // Qualquer lógica de inicialização/configuração que quero executar será colocada em uma fase do ciclo de vida que todo componente Angular possui
    this.userName = this.activatedRoute.snapshot.params.userName; // Através do ActivatedRoute tenho acesso ao valor que foi passado para a rota 
    this.photos2 = this.activatedRoute.snapshot.data["photos"]; // Pego data["photos"] do resolve definido no app.routing.module.ts      
  }
  
  load() {
    this.photoService.listFromUserPaginated(this.userName, ++this.currentPage) // Chamo o serviço do backend
    .subscribe(photos => {
      this.filter = ''
      this.photos2 = this.photos2.concat(photos); // o concat pega a lista de photos e concatena com outra lista, gerando uma nova lista e uma nova referência para o OnChanges detectar a mudança

      if(!photos.length) this.hasMore = false;
    });
  }
  
}
