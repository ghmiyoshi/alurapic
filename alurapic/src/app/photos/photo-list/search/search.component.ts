import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'ap-search',
    templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit, OnDestroy{
    
    @Output() onTyping: EventEmitter<string> = new EventEmitter<string>()
    @Input() valueSearch: string = ''
    debounce: Subject<string> = new Subject<string>();
    
    ngOnInit(): void { // Qualquer lógica de inicialização/configuração que quero executar será colocada em uma fase do ciclo de vida que todo componente Angular possui
        this.debounce.pipe(debounceTime(300)) // Só vou receber no subscribe o valor de um filter quando eu digitei e aguardei 300 milisegundos. Faço isso para não deixar que cada digito do input chegue no subscribe 
        .subscribe(filter => this.onTyping.emit(filter))
    }
    
    ngOnDestroy(): void { // Esse método é chamado toda vez que um objeto é destruído, ou seja, quando eu sair de PhotoListComponent e ir para outra rota ele será destruído
        this.debounce.unsubscribe(); // Preciso fazer isso para não alocar memória do observable do subject, se não, posso ter problema
    }

}