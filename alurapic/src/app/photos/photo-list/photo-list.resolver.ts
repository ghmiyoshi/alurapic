import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { PhotoService } from '../photo/photo.service';
import { Photo } from '../photo/photo';

/* O Resolver serve para resolver os dados durante a navegação de uma rota para disponibilizar para o componente, ou seja, 
no momento que a rota é ativada mas antes do componente ser carregado 
*/ 
@Injectable({
    providedIn: "root" // Indico que qualquer componente da aplicação que precisar de PhotoListResolver terá dispónivel a mesma instância 
})
export class PhotoListResolver implements Resolve<Observable<Photo[]>> {
    
    constructor (private service: PhotoService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Photo[]> {
        const userName = route.params.userName;

        return this.service.listFromUserPaginated(userName, 1);
    }
}