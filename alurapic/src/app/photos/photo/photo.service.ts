import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Photo } from "./photo";

const API = "http://localhost:3000"

@Injectable({ providedIn: "root" }) // Uso esse decorator @Injectable para serviço e digo o escopo "root", ou seja, qualquer componente da aplicação que precisar de PhotoService terá dispónivel a mesma instância 
export class PhotoService {

    constructor(private http: HttpClient) { }

    listFromUser(userName: string) {
        return this.http.get<Photo[]>(`${API}/${userName}/photos`); // Afirmo para o TS que vai vir um array de Object do backend
    }

    listFromUserPaginated(userName: string, page: number) {
        const parametro = new HttpParams().append('page', page.toString()) // Preciso converter number para string o parâmetro page

        return this.http.get<Photo[]>(`${API}/${userName}/photos`, { params: parametro });
    }

    upload(description: string, allowComments: boolean, file: File) {
        const formData = new FormData();
        formData.append('description', description)
        formData.append('allowComments', allowComments ? 'true' : 'false')
        formData.append('imageFile', file)

        return this.http.post(`${API}/photos/upload`, formData);
    }

    findById(id: string) {
        return this.http.get<Photo>(`${API}/photos/${id}`)
    }
}
