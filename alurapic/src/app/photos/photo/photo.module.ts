import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { PhotoComponent } from './photo.component';

@NgModule({
    declarations: [
        PhotoComponent
    ], 
    imports: [
        HttpClientModule, // Preciso importar o modulo HttpClientModule para usar no app.component.ts
        CommonModule // Importo CommonModule para usar as diretivas (ngFor, ngIf, etc) 
    ],
    exports: [
        PhotoComponent
    ]
})
export class PhotoModule {

}