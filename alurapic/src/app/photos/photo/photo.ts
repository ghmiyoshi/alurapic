export interface Photo { // Criei essa interface para tipar os dados retornados pela API

    id:number;
    postDate:Date;
    url:string;
    description:string;
    allowComments:boolean;
    likes:number;
    comments:number;
    userId:number;

}