import { Component, Input } from "@angular/core";

const CLOUD = 'http://localhost:3000/imgs/'

@Component({
    selector: "ap-photo",
    templateUrl: "photo.component.html"
})
export class PhotoComponent {
    private _url = '';
    
    @Input() set url(url: string){   // @Input esse decorator diz que a url e a descricao são inbound property, ou seja, o template (app.component.html) aceita receber os valores
        if(!url.startsWith('data')){ // Se a URI não começar com data 
            this._url = CLOUD + url
        } else {
            this._url = url
        }

    }

    @Input() descricao = "";

    get url() {
        return this._url
    }

}