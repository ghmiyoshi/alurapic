import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhotoService } from '../photo/photo.service';
import { Photo } from '../photo/photo';
import { ThrowStmt } from '@angular/compiler';
import { Observable } from 'rxjs';

@Component({
    templateUrl: './photo-details.component.html',
    styleUrls: ['photo-details.css']
})
export class PhotoDetailsComponent implements OnInit {

    photo$: Observable<Photo>

    constructor(
        private route: ActivatedRoute,
        private photoService: PhotoService
    ) { }

    ngOnInit(): void {
        const id = this.route.snapshot.params.photoId // Coloco photoId que foi o mesmo nome que defini em app.routing.module.ts
        console.log(id)

        this.photo$ = this.photoService.findById(id)
    }

}