import { NgModule } from '@angular/core';
import { SigninComponent } from './signin/signin.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home.component';
import { SignupService } from './signup/signup.service';

@NgModule({
    declarations: [
        SigninComponent,
        SignupComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule, // Esse módulo disponibiliza diretivas e recursos de validação para usar no formulário
        VMessageModule,
        RouterModule
    ],
    providers: [ // Se algum componente precisar dessa service injetado, ele vai estar dispónivel no meu HomeModule. Para todos os componentes declarados aqui  
        SignupService 
    ] 
})
export class HomeModule {

}