import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Router } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/platform-detector/platform-detector.service';

@Component({
    templateUrl: './signin.component.html'
})
export class SigninComponent implements OnInit {
    
    loginForm: FormGroup
    @ViewChild('userNameViewChild') userNameInput: ElementRef<HTMLInputElement> // Tipando o userNameInput
    
    constructor(
        private formBuilder: FormBuilder, 
        private authService: AuthService,
        private router: Router,
        private platformDetectorService: PlatformDetectorService
    ) {}
    
    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            userName: ['', Validators.required], // Campo obrigatório
            password: ['', Validators.required] // Campo obrigatório
        })

        this.userNameInput.nativeElement.focus()
    }

    login() {
        const userName = this.loginForm.get('userName').value // Pego o valor do username preenchido no loginForm
        const password = this.loginForm.get('password').value // Pego o valor do password preenchido no loginForm

        this.authService
            .authenticate(userName, password) // Chamo o serviço do backend
            .subscribe(
                () => {
                    this.router.navigate(['user', userName]) // Redirect o angular concatena /user/flavio
                    console.log('Autenticado!')
                },
                erro => {
                    console.log(erro)
                    this.loginForm.reset() // Limpo o formulário
                    this.platformDetectorService.isPlatformBrowser() && // Se for true e a plataforma de execução navegador executo o focus
                        this.userNameInput.nativeElement.focus()
                    alert('Usuário ou senha inválidos!') // Exibo alerta
                }
            )

    }

}