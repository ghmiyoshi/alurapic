import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserNotTakenValidatorService } from './user-not-taken.validator.service';
import { NewUser } from './new-user';
import { SignupService } from './signup.service';
import { Router } from '@angular/router';

@Component({
    templateUrl: 'signup.component.html',
    providers: [ UserNotTakenValidatorService ] // Quando esse componente for criado e precisar injetar UserNotTakenValidatorService, ele que vai prover esse serviço 
})
export class SignupComponent implements OnInit {

    signupForm: FormGroup
    @ViewChild('emailInputViewChild') emailInput: ElementRef<HTMLInputElement>

    constructor(
        private formBuilder: FormBuilder,
        private userNotTakenValidatorService: UserNotTakenValidatorService,
        private signupService: SignupService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.signupForm = this.formBuilder.group({
            email: ['',
                [
                    Validators.required,
                    Validators.email
                ]
            ],
            fullName: ['',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(40)
                ]
            ],
            userName: ['',
                [
                    Validators.required,
                    Validators.pattern(/^[a-z0-9_\-]+$/),
                    Validators.minLength(2),
                    Validators.maxLength(30)
                ],
                this.userNotTakenValidatorService.checkUserNameTaken()
            ],
            password: ['',
                [
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(14)
                ]
            ],
        })

        this.emailInput.nativeElement.focus()
    }

    signup() {
        const newUser = this.signupForm.getRawValue() as NewUser // Consigo um objeto JS com as propriedades email, full name, username, etc com os valores preenchidos.
        this.signupService
            .signup(newUser)
            .subscribe(() => {
                this.router.navigate(['']),
                error => console.log(error)
            })
    }

}