import { Injectable } from '@angular/core';
import { UserService } from '../user/user.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

/*
    Esse AuthGuard serve para direcionar o usuário para a lista de fotos quando estiver logado,
    se não deixo direciono para o login
*/


@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate { // Implemento a interface CanActivate para usar guarda de rotas
    
    constructor(private userService: UserService, private router: Router) {

    }

    canActivate(
        route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>{
        console.log('Ativou guarda de rotas Auth')
        
        if(!this.userService.islogged()) {
            this.router.navigate([''])
            return false
        }
        
        return true
    } 

}