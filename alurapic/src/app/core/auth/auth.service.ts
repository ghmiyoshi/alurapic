import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { UserService } from '../user/user.service';

const API_URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private userService: UserService  
  ) { }

  authenticate(userName: string, password: string) {
    return this.http.post(`${API_URL}/user/login`, {userName: userName, password: password}, {observe: 'response'}) // Preciso passar a configuração observe no post para acessar os headers na resposta
    .pipe(tap(res => { // Entre a execução da operação e o subscribe eu vou executar um código antes 
      const authToken = res.headers.get('x-access-token')
      this.userService.setToken(authToken)
      console.log(`User ${userName} authenticated with token ${authToken}`)
    })) 
  }

}
