import { Injectable } from '@angular/core';
import { TokenService } from '../token/token.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { User } from './user';
import * as jwt_decode from 'jwt-decode'

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private userSubject = new BehaviorSubject<User>(null) // O BehaviorSubject armazena a última emissão do dado até que alguém apareça para consumi-la.
    private userName: string
    
    constructor(private tokenService: TokenService) {
        this.tokenService.hasToken() && // Se tiver token eu executo o decodeAndNotify
            this.decodeAndNotify()
    }

    setToken(token: string) {
        this.tokenService.setToken(token); // Seto o token
        this.decodeAndNotify() // Faço o decode
    }

    decodeAndNotify() {
        const token = this.tokenService.getToken() // Pego o token que está salvo
        const user = jwt_decode(token) as User // Decodifico o token, pego o valor do payload e transformo em um tipo user
        
        this.userName = user.name
        this.userSubject.next(user) // Emito através do userSubject

    }

    getUser() {
        return this.userSubject.asObservable() // Quando chamo o getUser recebo um observable e vou poder fazer um subscribe
    }

    logout() {
        this.tokenService.removeToken() // Removo o token do local storage
        this.userSubject.next(null) // Seto null no userSubject para aparecer a msg de faça login no header
    }

    islogged() {
        return this.tokenService.hasToken();
    }

    getUsername() {
        return this.userName
    }
}