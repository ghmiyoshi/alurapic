import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PhotoListComponent } from "./photos/photo-list/photo-list.component";
import { PhotoFormComponent } from "./photos/photo-form/photo-form.component";
import { NotFoundComponent } from "./errors/not-found/not-found.component";
import { PhotoListResolver } from "./photos/photo-list/photo-list.resolver";
import { SigninComponent } from './home/signin/signin.component';
import { SignupComponent } from './home/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { LoginGuard } from './core/auth/login.guard';
import { AuthGuard } from './core/auth/auth.guard';
import { PhotoDetailsComponent } from './photos/photo-details/photo-details.component';

const rotas: Routes = [
    { 
        path: '', 
        component: HomeComponent,
        canActivate: [LoginGuard],
        children: [ // Será chamado componente HomeComponent que está associado com o path ''. A partir dai é procurado um match para um sub caminho, nesse caso '/signup'. Ou seja, além do HomeComponent também será chamado o SignupComponent
            {
                path: 'signup',
                component: SignupComponent,
            },
            { 
                path: "",
                component: SigninComponent
            },
        ]
    },
    { 
        path: "user/:userName", // Parametrizo um segmento de rota. :userName é uma varável onde o valor será recebido pela URL
        component: PhotoListComponent,
        resolve: {
            photos: PhotoListResolver
        } 
    }, 
    { 
        path: "photo/add",
        component: PhotoFormComponent,
        canActivate: [AuthGuard]
    },
    { 
        path: "photo/:photoId",
        component: PhotoDetailsComponent
    },
    { 
        path: "**",
        component: NotFoundComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(rotas) // Indico que tudo que vier "localhost:4200/" mais alguma coisa é a minha rota
    ],
    exports: [
        RouterModule // Com esse export, quem importar AppRoutingModule ganha o import de RouterModule tambem
    ]
    
})
export class AppRoutingModule {
    
}