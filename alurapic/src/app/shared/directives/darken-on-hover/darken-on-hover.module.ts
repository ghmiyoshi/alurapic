import { NgModule } from '@angular/core';
import { DarkenOnHverDirective } from './darken-on-hover.directive';

@NgModule({
    declarations: [DarkenOnHverDirective],
    exports: [DarkenOnHverDirective]
})
export class DarkenOnHoverModule {

}