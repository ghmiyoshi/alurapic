# alurapic

**01.** Bem começado, metade feito
 * Instalação do Angular CLI;
 * Criação de um novo projeto com a ferramenta e como executá-lo;
 * Compreensão da estrutura criada;
 * Data binding através de Angular Expression;
 * Data binding de atributos;
 * Convenções adotadas até o momento.

**02.** Criando o primeiro componente
 * Como o arquivo bootstrap.css pode ser adicionado ao processo de build do Angular CLI;
 * Criação de um novo componente;
 * A importância de declarar o componente em um módulo;
 * Como passar dados para o componente através das inbound properties;
 * Criação de um módulo e boas práticas;
 * A diretiva `*ngFor.

**03.** Integração com Web API's
 * Consumir uma Web API através do serviço HttpClient;
 * Injeção de dependência e a importância de providers;
 * Isolamento da lógica de acesso à Web API em classe de serviço;
 * Tipagem do retorno da API através de interface e sua vantagem;
 * Componentes possuem ciclo de vida;
 * A fase OnInit.

**04.** Single Page Applications e rotas
 * BrowserModule vs CommonModule;
 * Single Page Application e rotas no lado do navegador;
 * O módulo RouterModule;
 * A diretiva router-outlet como grande lacuna para exibição de outros componentes;
 * Módulo de rotas e definição de rotas;
 * Como lidar com páginas 404;
 * Parametrizando rotas e como obter valores do segmento parametrizado.

**05.** Novo componente, novos conceitos
 * Novo componente para listar photos;
 * Adequação dos dados recebidos pelo componente;
 * Quando a fase OnInit não é suficiente;
 * A interface OnChanges, e como interagir com SimpleChanges.



